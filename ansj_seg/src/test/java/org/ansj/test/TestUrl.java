package org.ansj.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.ansj.domain.Result;
import org.ansj.splitWord.analysis.NlpAnalysis;

public class TestUrl {
	
	public static void main(String[] args){
		
		FileOutputStream fop = null;
		File out = new File("G:/迅雷下载/url.txt");
		String files = "G:/迅雷下载/新聞博客_detail/hbase_blog_detail.txt";//原文
		try {
			fop = new FileOutputStream(out,true);
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new FileReader(new File(files)));
			String line;
			String str=null;
			line = br.readLine();
			String url="";
			Set<String> set=new HashSet<String> ();
			long index=0;
			while (null != line) {
				if(line.equals("（完）")){
					line = br.readLine();
					continue;
				}
				if(line.indexOf("SiteHost")<0){
					System.out.println(index++ +" | "+line);
					line = br.readLine();
					continue;
				}
				url=line.substring(line.indexOf("SiteHost"));
//				if(index++ % 10000==0 ){
//					System.out.println(index++ +" | "+url);
//				}
				url=url.substring(11,url.indexOf(",")-1 );
				//System.out.println(url);
				set.add(url);
//				fop.write(("原文:"+line).getBytes());
//				fop.write("\t\n".getBytes());
				
				
//				fop.write(("分词:"+str).getBytes());
//				fop.write("\t\n".getBytes());
//				fop.flush();
				line = br.readLine();
			}
			for(String strs:set){
				System.out.println(strs);
				fop.write((strs+"\n").getBytes());
			}
			fop.flush();
			fop.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
