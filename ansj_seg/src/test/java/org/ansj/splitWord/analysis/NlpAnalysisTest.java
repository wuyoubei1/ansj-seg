package org.ansj.splitWord.analysis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.ansj.CorpusTest;
import org.ansj.domain.Result;
import org.ansj.domain.Term;
import org.junit.Test;

public class NlpAnalysisTest extends CorpusTest {

	public void test() {
		for (String string : lines) {
			System.out.println(NlpAnalysis.parse(string));
		}
	}
	
	public void split() {
		FileOutputStream fop = null;
		File file = new File("G:/迅雷下载/hbaset_result.txt");
		String files = "G:/迅雷下载/txt/result-full.txt";//原文
		try {
			fop = new FileOutputStream(file,true);
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new FileReader(new File(files)));
			String line;
			String str=null;
			line = br.readLine();
			int end = 0;
			int start = 0;
			while (null != line) {

				start = line.indexOf("Content");
				if (start < 0) {
					line = br.readLine();
					continue;
				}
				end = line.indexOf("CrawlTime");
				if (end < 0) {
					line = br.readLine();
					continue;
				}
				line = line.substring(start + 10, end - 3).replace(" ", "").replace("    ", "").replace(" ", "");
				while (line.contains(" ")) {
					line = line.replaceAll("\\s*", "");
				}
				if ("".equals(line)) {
					line = br.readLine();
					continue;
				}
				Result rs = NlpAnalysis.parse(line);
				//System.out.println("原文：" + line);
				//System.out.println("分词：" + rs.toString());
				
				fop.write(("原文:"+line).getBytes());
				fop.write("\t\n".getBytes());
				
				str=baseAnalysis(rs.toString());
				
				fop.write(("分词:"+str).getBytes());
				fop.write("\t\n".getBytes());
				fop.flush();
				line = br.readLine();
			}
			fop.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
	
	@Test
	public void resultFull(){
		FileOutputStream fop = null;
		File file = new File("G:/迅雷下载/111.txt");
		String files = "F:/document/学习/ansj/1111.txt";//原文
		try {
			fop = new FileOutputStream(file,true);
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new FileReader(new File(files)));
			String line;
			String str=null;
			line = br.readLine();
			while (null != line) {
				if(line.equals("（完）")){
					line = br.readLine();
					continue;
				}
				Result rs = NlpAnalysis.parse(line);
				
				fop.write(("原文:"+line).getBytes());
				fop.write("\t\n".getBytes());
				
				str=rs.toString();
				
				str=baseAnalysis(str);
				
				fop.write(("分词:"+str).getBytes());
				fop.write("\t\n".getBytes());
				fop.flush();
				line = br.readLine();
			}
			fop.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * baseAnalysis分析
	 * @param str
	 * @return
	 */
	public String baseAnalysis(String str){
		System.out.println("待分析的字符串："+str);
		String temps="";
		int end=str.indexOf("/nw");
		if(end<0){
			temps=str;
		}else{
			String temp=str.substring(0, end);
			int start=temp.lastIndexOf(",");
			//temp=temp.substring(start+1);
			//System.out.println(temp);
			Result rs=BaseAnalysis.parse(temp.substring(start+1));
			if(checkMorMq(rs)){//如果结果中全是 m或者mq类型
				String as=rs.toString();
				temps=temp.substring(0, start+1)+as;
			}else{
				temps=str.substring(0, end+3);
			}
			//System.out.println(temps);
			temps+=baseAnalysis(str.substring(end+3));
		}
		System.out.println("结果："+temps);
		return temps;
	}
	
	/**
	 * 验证是否全部为m或者mq * true 全部是  false不是**/
	public boolean checkMorMq(Result rs){
		boolean flag=true;
		String natureStr="";
		for(Term term:rs.getTerms()){
			natureStr=term.getNatureStr();
			System.out.println("词性："+natureStr+"|"+term.toString());
			if(!natureStr.equals("m")&&!natureStr.equals("mq")&&!natureStr.equals("q")&&!natureStr.equals("t")){
				flag=false;
				break;
			}
		}
		return flag;
	}
	
	//@Test
	public void ssss(){
		String str="四千四百四十分之一个小时/nw,的朋友/nw,4月/t,在德国/nw,出差/nw";
		System.out.println(str);
		String ttt=baseAnalysis(str);
		System.out.println("结果："+ttt);
	}
	
	public void split2(){
		FileWriter fw=null;
		try {
			fw=new FileWriter("G:/迅雷下载/result.txt");
			//fw.write(cbuf);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
